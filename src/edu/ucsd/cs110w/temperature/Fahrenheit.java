package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wbv
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{

		return "" + getValue() + " F";
	}
	@Override
	public Temperature toCelsius() {

		return new Celsius((float) ((getValue()-32)/1.8));
	}
	@Override
	public Temperature toFahrenheit() {

		return new Fahrenheit(getValue());
	}
	@Override
	public Temperature toKelvin() {

		return new Kelvin ((float) (((getValue() -32)*5)/9 +273.15));
	}
}
