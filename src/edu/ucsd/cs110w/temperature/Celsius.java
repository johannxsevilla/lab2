/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wbv
 *
 */
public class Celsius extends Temperature
{
	
	public Celsius(float t)
	{
		super(t);
	}
	
	public String toString()
	{
		return " " + getValue() + " C ";
	}

	public Temperature toCelsius() {
		return new Celsius(getValue());
	}
	
	public Temperature toFahrenheit() {
		return new Fahrenheit((float) (getValue()*1.8+32));
	}

	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return new Kelvin ((float) (getValue()+ 273.15));
	}
}
