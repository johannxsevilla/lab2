/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (cs110wbv): write class javadoc
 *
 * @author cs110wbv
 */
public class Kelvin extends Temperature {

	public Kelvin(float t)
	{
		super(t);
	}
	public String toString()
	{
      
		return "" + getValue() + " K";
	}
	@Override
	public Temperature toCelsius() {

		return new Celsius ( (float) (getValue() - 273.15));
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO: Complete this method
		return new Fahrenheit ((float) ((getValue() - 273.15)*1.8 +32));
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return new Kelvin (getValue());
	}
}
